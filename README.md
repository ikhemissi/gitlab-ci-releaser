# gitlab-ci-releaser

> Yet another conventional GitLab releaser : find unreleased changes, bump version, commit/push changes and create a new GitLab release with notes

## Installation

The setup requires Node 6+.

1. Add a new release stage in your [.gitlab-ci.yml](https://docs.gitlab.com/ce/ci/yaml/README.html).
2. Install `gitlab-ci-releaser` using `npm install gitlab-ci-releaser`
3. Add the environment variables `GITLAB_CI_RELEASER_NAME`, `GITLAB_CI_RELEASER_EMAIL` and `GITLAB_CI_RELEASER_TOKEN` with settings of a user having `Developer` permissions and the permission to push to the release branch (e.g. `master`).
4. Optionally, add an environment variable `NPM_TOKEN` if you want to publish your packages to a npm registry.

I recommend creating and using a new user with `Developer` + push permissions, adding the user to the project members and generating all the environment variables using this user account (the SSH access permissions are not needed for this account).

## Usage

You can use the `gitlab-ci-releaser` command to create releases:

```sh
$(npm bin)/gitlab-ci-releaser
```

To publish to npm, add the `npm` option:
```sh
$(npm bin)/gitlab-ci-releaser --npm
```

You can optionally pass the npm registry and authentication token via command line arguments:
```sh
$(npm bin)/gitlab-ci-releaser --token "<gitlab auth token>" --npmToken "<npm auth token>" --npmRegistry "http://localhost:4873/"
```
This being said, I strongly recommend using environment variables for this sensitive information instead of passing them as program arguments.

Here is an example of GitLab CI stage for releasing with npm publishing.
```
image: node:6

stages:
  - build
  - release

build:
  stage: build
  script:
    - npm install

release:
  stage: release
  before_script:
    - npm install gitlab-ci-releaser
  only:
    - master
  script:
    - $(npm bin)/gitlab-ci-releaser --npm
  when: manual
```

## Environment variables
`gitlab-ci-releaser` relies on many environment variables:

| Name                     | Type      | Set by    | Description                                             | Example                                                                                    |
|--------------------------|-----------|-----------|---------------------------------------------------------|--------------------------------------------------------------------------------------------|
| `GITLAB_CI_RELEASER_NAME`  | Mandatory | Admin     | GitLab account username, used to create release commits | ikhemissi                                                                                  |
| `GITLAB_CI_RELEASER_EMAIL` | Mandatory | Admin     | GitLab account email, used to create release commits    | mail.address@example.com                                                                   |
| `GITLAB_CI_RELEASER_TOKEN` | Mandatory | Admin     | GitLab account access token, used to push commits       | aBcde1234AbcD5678ef                                                                        |
| `NPM_TOKEN`                | Optional  | Admin     | NPM authToken. Only needed when publishing to NPM.      | 9febf68b-f1b7-5bd9-701a-576eba203a1f3                                                      |
| `NPM_REGISTRY`             | Optional  | Admin     | NPM registry. By default, it will be fetched from npm configuration     | http://private.sinopia.registry:4873/                                                      |
| `CI_PROJECT_URL`           | Mandatory | GitLab CI | Project url                                             | https://gitlab.com/ikhemissi/test-release-project                                          |
| `CI_PROJECT_PATH`          | Mandatory | GitLab CI | Project ID (user name + project name)                   | ikhemissi/test-release-project                                                             |
| `CI_BUILD_REPO`            | Mandatory | GitLab CI | Project repository                                      | https://gitlab-ci-token:xxxx@gitlab.com/ikhemissi/test-release-project.git |
| `CI_BUILD_REF_NAME`        | Mandatory | GitLab CI | Project branch used in the build                        | master                                                                                     |
| `GITLAB_USER_EMAIL`        | Optional  | GitLab CI | Email of the user who triggered the build               | mail.address@example.com                                                                   |
| `CI_BUILD_ID`              | Optional  | GitLab CI | Internal GitLab build number                            | 50                                                                                         |

## Release versions
By default, `gitlab-ci-releaser` uses [semantic commit](https://seesparkbox.com/foundry/semantic_commit_messages) types and footers to determine the next release [version](http://semver.org/):
- if this is the first release, then the version `1.0.0` will be used to create the release
- if the new changes include a commit with a breaking change (the commit message contains `BREAKING CHANGE`) then a major release will be created
- if there is at least one feature commit (commit type = `feat`), then a minor release will be created
- otherwise, a patch release will be created

This being said, you can force a specific release type or version as long as they conform to the [semver spec](http://semver.org/) using the following options:
- `releaseType`:
This option allows you to force a specific release version increment (e.g. major, minor, patch).
For example, by running `gitlab-ci-releaser --releaseType major` we get a version `2.0.0` if the previous one was `1.5.0`, and we get `4.0.0` if the previous one was `3.0.0`.
- `releaseVersion`:
With this option, you can force using a specific release version (e.g. 1.9.1).
For example, by running `gitlab-ci-releaser --releaseVersion 1.9.1` we create a release having the version `1.9.1` **even if the previous one was `2.0.0`, which may lead to potential issues**.


Please note that the semantic commits are also used to generate the release notes, so I don't recommend using the `releaseType` and `releaseVersion` unless your project do not use semantic commits.

If you plan to switch to semantic commits, I strongly suggest using a tool like [commitizen](https://www.npmjs.com/package/commitizen) to create your commits and have better commit messages.

## Similar projects
- [semantic-release-gitlab](https://www.npmjs.com/package/semantic-release-gitlab): this project inspired me to write gitlab-ci-releaser. Many thanks to Hutson.
- [gitlab-release](https://www.npmjs.com/package/gitlab-release)

## Contributing
Please follow the [Airbnb guidelines](https://github.com/airbnb/javascript) and commit your changes with [commitzen](https://www.npmjs.com/package/commitizen) using `git cz`.
