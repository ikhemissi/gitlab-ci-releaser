/* eslint-disable no-param-reassign no-underscore-dangle */
'use strict';

const test = require('ava');
const proxyquire = require('proxyquire');
const { Readable } = require('stream');

function mockRawCommits(commitList) {
  const readable = new Readable();
  readable._read = () => {};
  commitList.forEach((commit) => {
    readable.push(commit);
  });
  readable.push(null);
  readable.emit('close');
  return readable;
}

test('must return a list of all the commits', (t) => {
  const commits = proxyquire('../../src/repository/commits', {
    'git-raw-commits': () => mockRawCommits([
      `docs: Provide more details about a feature

-hash-
9278b34cd897e0599524f5bae292796d3ee9a7a2`,
      `feat: Add a feature

More detailed feature description

-hash-
ca587494372cedd7adc844ada9acdf61295911cf`,
    ]),
  });

  return commits.getCommitList()
    .then((list) => {
      t.deepEqual(list, [
        {
          type: 'docs',
          scope: null,
          subject: 'Provide more details about a feature',
          merge: null,
          header: 'docs: Provide more details about a feature',
          body: null,
          footer: null,
          notes: [],
          references: [],
          mentions: [],
          revert: null,
          hash: '9278b34cd897e0599524f5bae292796d3ee9a7a2',
        },
        {
          type: 'feat',
          scope: null,
          subject: 'Add a feature',
          merge: null,
          header: 'feat: Add a feature',
          body: 'More detailed feature description',
          footer: null,
          notes: [],
          references: [],
          mentions: [],
          revert: null,
          hash: 'ca587494372cedd7adc844ada9acdf61295911cf',
        },
      ]);
    });
});

test('must return an empty list if there are no commits', (t) => {
  const commits = proxyquire('../../src/repository/commits', {
    'git-raw-commits': () => mockRawCommits([]),
  });

  return commits.getCommitList()
    .then((list) => {
      t.deepEqual(list, []);
    });
});

test('must return an empty list if there is only a version bump commit', (t) => {
  const commits = proxyquire('../../src/repository/commits', {
    'git-raw-commits': () => mockRawCommits([
      `1.3.5

-hash-
e384f312e96f10920e8d5478fc74db8a1884c35e`,
    ]),
  });

  return commits.getCommitList()
    .then((list) => {
      t.deepEqual(list, []);
    });
});
