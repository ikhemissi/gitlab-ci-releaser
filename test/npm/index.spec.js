/* eslint-disable no-param-reassign */
'use strict';

const test = require('ava');
const stubAsPromised = require('../helpers/stubAsPromised');
const commands = require('../../src/utils/commands');
require('../helpers/mockPackageJson');

test.beforeEach((t) => {
  t.context.runCommand = commands.run;
  commands.run = stubAsPromised({ published: true });
});

test.afterEach((t) => {
  commands.run = t.context.runCommand;
});

test('must execute "npm publish"', (t) => {
  const npm = require('../../src/npm');
  return npm.publish()
    .then(() => {
      t.true(commands.run.calledOnce);
      t.true(commands.run.calledWithExactly('npm publish'));
    });
});
