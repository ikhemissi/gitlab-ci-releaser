'use strict';

const sinon = require('sinon');

function stubAsPromised(data) {
  return sinon.stub().returns(Promise.resolve(data));
}

module.exports = stubAsPromised;
