'use strict';

const fs = require('fs');

const originalReadFile = fs.readFile;
const fakePackageJson = {
  name: 'dummy-project',
  version: '1.0.0',
  repository: {
    url: 'git+ssh://git@gitlab.com/ikhemissi/dummy-project.git',
  },
};

fs.readFile = (path, format, cb) => {
  if (path && path.endsWith('package.json')) {
    cb(null, JSON.stringify(fakePackageJson, null, 2));
  } else {
    originalReadFile(path, format, cb);
  }
};

module.exports = fakePackageJson;
