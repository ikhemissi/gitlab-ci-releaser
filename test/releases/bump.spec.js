/* eslint-disable no-param-reassign */
'use strict';

const test = require('ava');
const sinon = require('sinon');
const path = require('path');
const stubAsPromised = require('../helpers/stubAsPromised');
const mockPackageJson = require('../helpers/mockPackageJson');

const fs = require('fs');
const repository = require('../../src/repository');

test.beforeEach((t) => {
  t.context.commitAndPush = repository.commitAndPush;
  repository.commitAndPush = stubAsPromised('commithash');
  sinon.stub(fs, 'writeFile', (path, data, cb) => { cb(null); });
});

test.afterEach((t) => {
  repository.commitAndPush = t.context.commitAndPush;
  fs.writeFile.restore();
});

test('must update package.json, commit and push changes', (t) => {
  const bump = require('../../src/releases/bump');
  const nextVersion = '1.1.0';
  const expectedNewPackageData = Object.assign({}, mockPackageJson, { version: nextVersion });
  const expectedNewPackageJson = JSON.stringify(expectedNewPackageData, null, 2);
  const packageJsonPath = path.join(process.cwd(), 'package.json');
  return bump(nextVersion) // from 1.0.0 to 1.1.0
    .then((updateCommitHash) => {
      t.true(fs.writeFile.calledOnce);
      t.true(fs.writeFile.calledWith(packageJsonPath, expectedNewPackageJson));
      t.true(repository.commitAndPush.calledOnce);
      t.true(repository.commitAndPush.calledWithExactly(nextVersion, 'package.json'));
      t.is(updateCommitHash, 'commithash');
    });
});
