/* eslint-disable no-param-reassign */
'use strict';

const test = require('ava');
const sinon = require('sinon');
require('../helpers/mockPackageJson');
const notes = require('../../src/releases/notes');
const repository = require('../../src/repository');
const fs = require('fs');

test.beforeEach(() => {
  sinon.stub(fs, 'writeFile', (path, data, cb) => { cb(null); });
  sinon.stub(notes, 'generate', () => 'release notes');
  sinon.stub(repository, 'commitAndPush', () => Promise.resolve('commithash'));
});

test.afterEach(() => {
  fs.writeFile.restore();
  notes.generate.restore();
  repository.commitAndPush.restore();
});

test.serial('must update package.json, generate notes and push changes', (t) => {
  const releases = require('../../src/releases');
  const fakeCommits = [{ commit: 1, notes: [], type: 'feat' }, { commit: 2, notes: [] }];
  return releases.createNewRelease({ version: '1.0.0', commits: fakeCommits }) // from 1.0.0 to 1.1.0
    .then(({ hash, version, notes: releaseNotes }) => {
      t.is(hash, 'commithash');
      t.is(version, '1.1.0');
      t.is(releaseNotes, 'release notes');
    });
});

test.serial('must use the provided "next release version"', (t) => {
  const releases = require('../../src/releases');
  const fakeCommits = [{ commit: 1, notes: [], type: 'feat' }, { commit: 2, notes: [] }];
  return releases.createNewRelease({ version: '1.0.0', commits: fakeCommits, releaseVersion: '1.3.0' }) // from 1.0.0 to 1.3.0
    .then(({ hash, version, notes: releaseNotes }) => {
      t.is(hash, 'commithash');
      t.is(version, '1.3.0');
      t.is(releaseNotes, 'release notes');
    });
});

test.serial('must throw if the provided "next release version" is not valid', (t) => {
  const releases = require('../../src/releases');
  const fakeCommits = [{ commit: 1, notes: [], type: 'feat' }, { commit: 2, notes: [] }];
  return releases.createNewRelease({ version: '1.0.0', commits: fakeCommits, releaseVersion: '1.x.0' })
    .catch((err) => {
      t.is(err.message, 'The version 1.x.0 is not a valid semver!');
    });
});

test.serial('must use the provided "next release type"', (t) => {
  const releases = require('../../src/releases');
  const fakeCommits = [{ commit: 1, notes: [], type: 'feat' }, { commit: 2, notes: [] }];
  return releases.createNewRelease({ version: '1.0.0', commits: fakeCommits, releaseType: 'major' }) // from 1.0.0 to 2.0.0
    .then(({ hash, version, notes: releaseNotes }) => {
      t.is(hash, 'commithash');
      t.is(version, '2.0.0');
      t.is(releaseNotes, 'release notes');
    });
});

test.serial('must use the provided "next release type"', (t) => {
  const releases = require('../../src/releases');
  const fakeCommits = [{ commit: 1, notes: [], type: 'feat' }, { commit: 2, notes: [] }];
  return releases.createNewRelease({ version: '1.0.0', commits: fakeCommits, releaseType: 'unknown' })
    .catch((err) => {
      t.is(err.message, 'The release increment level unknown is not valid!');
    });
});
