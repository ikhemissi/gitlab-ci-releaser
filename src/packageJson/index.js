'use strict';

const fs = require('fs');
const path = require('path');
const promisify = require('es6-promisify');

const readFile = promisify(fs.readFile);
const packagePath = path.join(process.cwd(), 'package.json');
const packageInfo = readFile(packagePath, 'utf8').then(JSON.parse);

function getContent() {
  return packageInfo;
}

function getPath() {
  return packagePath;
}

module.exports = {
  getContent,
  getPath,
};
