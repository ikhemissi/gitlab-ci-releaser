'use strict';

const registryUrl = require('registry-url');
const { run: runCommand } = require('../utils/commands');

function setupConfig(authToken, npmRegistry) {
  let setupPromise = Promise.resolve();
  if (authToken) {
    const fullRegistryUrl = npmRegistry || registryUrl();
    const registry = fullRegistryUrl.replace(/^https?:/gi, '');
    setupPromise = runCommand(`npm config set "${registry}:_authToken" ${authToken}`);
  }

  return setupPromise;
}

function publish(authToken, npmRegistry) {
  return setupConfig(authToken, npmRegistry)
    .then(() => runCommand('npm publish'));
}

module.exports = {
  publish,
};
