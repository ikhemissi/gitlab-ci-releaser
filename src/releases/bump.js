'use strict';

const fs = require('fs');
const promisify = require('es6-promisify');
const repository = require('../repository');
const packageJson = require('../packageJson');

const writeFile = promisify(fs.writeFile);

function updateVersion(newVersion) {
  return packageJson.getContent()
    .then(contents => Object.assign({}, contents, { version: newVersion }))
    .then(contents => JSON.stringify(contents, null, 2))
    .then(contents => writeFile(packageJson.getPath(), contents))
    .then(() => newVersion);
}

// Bump version: update package.json, commit and push changes
function bump(newVersion) {
  return updateVersion(newVersion)
    .then(() => repository.commitAndPush(newVersion, 'package.json'));
}

module.exports = bump;
