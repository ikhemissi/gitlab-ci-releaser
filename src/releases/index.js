'use strict';

const semver = require('semver');
const notes = require('./notes');
const bump = require('./bump');

function nextReleaseType(commitList) {
  const breakingChanges = commitList.filter(commit => commit.notes.length);
  const newFeatures = commitList.filter(commit => commit.type === 'feat');
  let releaseType = null;

  if (breakingChanges.length) {
    releaseType = 'major';
  } else if (newFeatures.length) {
    releaseType = 'minor';
  } else if (commitList.length) {
    releaseType = 'patch';
  }

  return releaseType;
}

function nextReleaseVersion({ version, commits, releaseType, releaseVersion }) {
  if (releaseVersion) { // forced release version
    if (!semver.valid(releaseVersion)) {
      throw new Error(`The version ${releaseVersion} is not a valid semver!`);
    }

    return releaseVersion;
  } else if (!version) { // first release
    return '1.0.0';
  } else if (releaseType) { // forced release type (semver increment level)
    const nextVersion = semver.inc(version, releaseType);
    if (!nextVersion) {
      throw new Error(`The release increment level ${releaseType} is not valid!`);
    }

    return nextVersion;
  }

  return semver.inc(version, nextReleaseType(commits)); // determine the increment from commit messages
}

function createNewRelease({ version, commits, releaseType, releaseVersion }) {
  if (Array.isArray(commits) && !commits.length) {
    const error = new Error('No changes since last release');
    error.type = 'NO_CHANGES';
    throw error;
  }

  return Promise.resolve()
    .then(() => nextReleaseVersion({ version, commits, releaseType, releaseVersion }))
    .then(nextVersion => Promise.all([notes.generate(commits, nextVersion), bump(nextVersion)])
      .then(([releaseNotes, hash]) => ({ hash, version: nextVersion, notes: releaseNotes })));
}

module.exports = {
  createNewRelease,
};
