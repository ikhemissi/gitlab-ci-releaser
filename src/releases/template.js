'use strict';

module.exports = `
# v{{version}}

{{#each changes}}

## {{label}}
{{#each list}}
* {{hash}} {{subject}}{{#if issues}} {{issues}}{{/if}}
{{/each}}

{{/each}}
`;
