'use strict';

const releases = require('./releases');
const repository = require('./repository');
const gitlabci = require('./gitlabci');
const npm = require('./npm');

const {
  GITLAB_CI_RELEASER_TOKEN,
  NPM_TOKEN,
  NPM_REGISTRY,
} = process.env;

function release({ releaseType, releaseVersion, withNpm, token = GITLAB_CI_RELEASER_TOKEN, npmToken = NPM_TOKEN, npmRegistry = NPM_REGISTRY }) {
  return repository.getLastVersion()
    .then((details) => {
      const version = details && details.version;
      const hash = details && details.hash;
      return Promise.all([
        Promise.resolve(version),
        repository.getCommitList(hash || ''),
      ]);
    })
    .then(([version, commits]) => releases.createNewRelease({ version, commits, releaseType, releaseVersion }))
    .then(({ version, notes, hash }) => {
      const promises = [
        gitlabci.addRelease({ token, hash, notes, version }),
      ];

      if (withNpm) {
        promises.push(npm.publish(npmToken, npmRegistry));
      }

      return Promise.all(promises)
        .then(() => version);
    });
}

module.exports = {
  release,
};
